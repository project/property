core = 7.x
api = 2

defaults[projects][subdir] = "contrib"

; Contrib modules

projects[admin_menu][version] = "3.0-rc5"
projects[adminimal_admin_menu][version] = "1.7"
projects[admin_views][version] = "1.6"
projects[addressfield][version] = "1.2"
projects[crumbs][version] = "2.5"
projects[ctools][version] = "1.10"
projects[ckeditor][version] = "1.17"
projects[chosen][version] = "2.0-beta5"
projects[date][version] = "2.9"
projects[ds][version] = "2.14"
projects[diff][version] = "3.2"
projects[eva][version] = "1.3"
projects[entity][version] = "1.7"
projects[entitycache][version] = "1.5"
projects[entityreference][version] = "1.1"
projects[entityreference][patch][] = "http://drupal.org/files/1580348-universal-formatters-17.patch"
projects[entityform][version] = "2.0-rc1"
projects[elements][version] = "1.4"
projects[features][version] = "2.10"
projects[feeds][version] = "2.0-beta2"
projects[feeds_tamper][version] = "1.1"
projects[field_group][version] = "1.5"
projects[file_entity][version] = "2.0-beta3"
projects[filefield_paths][version] = "1.0"
projects[globalredirect][version] = "1.5"
projects[google_analytics][version] = "2.3"
projects[hierarchical_select][version] = "3.0-beta7"
projects[inline_entity_form][version] = "1.8"
projects[insert][version] = "1.3"
projects[image_delta_formatter][version] = "1.0-rc1"
projects[jquery_update][version] = "2.7"
projects[job_scheduler][version] = "2.0-alpha3"
projects[link][version] = 1.4
projects[libraries][version] = "2.3"
projects[linkchecker][version] = "1.3"
projects[linkit][version] = "3.5"
projects[login_destination][version] = "1.4"
projects[mailsystem][version] = 2.34
projects[mailsystem][patch][1534706] = "https://www.drupal.org/files/mailsystem.1534706.6.patch"
projects[mimemail][version] = 1.0-beta4
projects[migrate][version] = 2.8
projects[migrate_extras][version] = 2.5
projects[migrate_extras][patch][] = "http://drupal.org/files/migrate_extras-fix-destid2-array-1951904-4.patch"
projects[multiform][version] = "1.1"
projects[media][version] = "2.0-beta2"
projects[media_youtube][version] = "3.0"
projects[media_vimeo][version] = "2.1"
projects[menu_block][version] = "2.7"
projects[menu_attributes][version] = 1.0
projects[metatag][version] = "1.17"
projects[minisite][version] = "1.0-alpha1"
projects[module_permissions][version] = "1.1"
projects[module_filter][version] = 2.0
projects[panels][version] = "3.7"
projects[pathauto][version] = 1.3
projects[pathologic][version] = "3.1"
projects[plupload][version] = 1.7
projects[quicktabs][version] = "3.6"
projects[redirect][version] = "1.0-rc3"
projects[rules][version] = "2.9"
projects[r4032login][version] = "1.8"
projects[sharethis][version] = "2.13"
projects[services][version] = "3.17"
projects[strongarm][version] = "2.0"
projects[token][version] = "1.6"
projects[taxonomy_menu][version] = 1.5
projects[taxonomy_autolink][version] = "1.x-dev"
projects[taxonomy_csv][version] = "5.10"
projects[title][version] = "1.0-alpha8"
projects[transliteration][version] = "3.2"
projects[uuid][version] = "1.0-beta2"
projects[uuid_features][version] = "1.0-alpha4"
projects[variable][version] = 2.5
projects[views][version] = "3.14"
projects[views_megarow][version] = "1.6"
projects[views_bulk_operations][version] = "3.3"
projects[views_data_export][version] = "3.0-beta9"
projects[workbench][version] = "1.2"
projects[workbench_access][version] = "1.4"
projects[workbench_moderation][version] = "1.4"
projects[xmlsitemap][version] = "2.3"

; Internationalization

projects[i18n][version] = "1.13"
projects[l10n_update][version] = 2.0

; Security

projects[captcha][version] = "1.3"
projects[honeypot][version] = "1.22"
projects[login_security][version] = "1.9"
projects[logintoboggan][version] = "1.5"
projects[password_policy][version] = "1.12"
projects[paranoia][version] = "1.6"
projects[recaptcha][version] = "2.0"
projects[seckit][version] = "1.9"
projects[securepages][version] = "1.0-beta2"
projects[spamspan][version] = "1.2"
projects[shield][version] = "1.2"
projects[username_enumeration_prevention][version] = "1.2"

; Search related modules.

projects[facetapi][version] = 1.5
projects[facetapi][patch][] = "https://drupal.org/files/1616518-term_remove_link-24.patch"
projects[facetapi][patch][2378693] = "https://www.drupal.org/files/issues/notice_undefined-2378693-3.patch"
projects[search_api][version] = "1.20"
projects[search_api_db][version] = 1.5
projects[search_api_page][version] = "1.3"
projects[search_api_solr][version] = "1.9"
projects[search_api_ranges][version] = 1.5
projects[search_api_ranges][patch][] = "https://drupal.org/files/issues/search_api_ranges-rewrite-data-alteration-callback-2001846-4.patch"
projects[search_api_sorts][version] = 1.6
projects[search404][version] = "1.4"

; Themes

projects[adminimal_theme][version] = "1.24"